/*
    Лабораторная работа №1. Вариант 15. Перемитин Александр.
    Дано двузначное натуральное число, в котором все цифры различны.
    А) Определить его максимальную и минимальную цифры.
    Б) Определить, на сколько его максимальная цифра превышает минимальную. Чистло задавать генератором случайных чисел.
 */
import java.util.Random; //Импорт пакета для рандомизации
public class helloWorld { //Класс hello world
    public static void main(String[] args) { //Метод main
        int inputData=11, firstNumber=0, secondNumber=0;
        boolean key = false;
        while (firstNumber == secondNumber) {
            Random rnd = new Random(System.currentTimeMillis()); //Инициализация рандома
            inputData = rnd.nextInt(89)+10; //Вбиваем переменные
            firstNumber = inputData / 10; //Разделяем двузначное число на 2 цифры
            secondNumber = inputData % 10; //Разделяем двузначное число на 2 цифры
            if (firstNumber != secondNumber){
                key = true;
            }
        }
        if (key == true) {
            System.out.print("Исходное число: ");
            System.out.println(inputData); //Выводим исходное число
            if ((inputData > 9) && (inputData < 100)) { //Проверяем на корректность ввода
                if (firstNumber > secondNumber) { //Если первая цифра больше второй
                    System.out.print("Первая цифра больше второй на: ");
                    System.out.println(firstNumber - secondNumber);
                } else { //Если вторая цифра больше первой
                    System.out.print("Вторая цифра больше правой на: ");
                    System.out.println(secondNumber - firstNumber);
                }
            } else { //Если число недвузначное
                System.out.println("Введено недвузначное число");
            }
        }
    } //Конец тела метода
} //Конец тела класса