import java.util.Stack;
class Monitor{
    public String mName; public int mInches; public int mPrice;
    public Monitor(String name, int inches, int price){
        mName = name; mInches = inches; mPrice = price;
    }
    @Override
    public String toString(){ return this.mName; }
    public int toInt(){ return this.mInches; }
}
class lab6_list{
    public static void main(String[] args){
        Monitor first  = new Monitor("ASUS", 17, 2000);
        Monitor second = new Monitor("SAMSUNG", 22, 2500);
        Monitor third  = new Monitor("BENQ", 27, 4000);
        Stack <Monitor> screen = new Stack<>(); screen.push(first); screen.push(second); screen.push(third);
        System.out.println("Текущий стек: " + screen);
        if (third.mInches > 20){ System.out.println(screen.peek() + " "); } screen.pop();
        if (second.mInches > 20){ System.out.println(screen.peek() + " "); } screen.pop();
        if (first.mInches > 20){ System.out.println(screen.peek() + " "); } screen.pop();
        System.out.println("########################");
    }
}