/*  Создать стек, информационными полями которого являются: монитор, диагональ и его цена.
    Добавить в стек сведения о новом мониторе. Организовать просмотр данных стека
    и определить количество мониторов с диагональю более 20 дюймов. */
class Stack {
    private int mSize; private int top; 
    private int[] stackBrand; private int[] stackInches; private int[] stackPrice;
    //Конструктор
    public Stack(int m){
        this.mSize = m; 
        stackBrand  = new int[mSize]; stackInches = new int[mSize]; stackPrice  = new int[mSize];
        top = -1;
    }
    //Метод добавления элемента
    public void addElement(int element, int inches, int price) {
        stackBrand[++top] = element; stackInches[top] = inches; stackPrice[top] = price;
    }
    //Метод удаления элемета
    public int deleteElement() { return --top; }
    //Метод вывести в консоль стек
    public void readStack() { 
        while (top != -1){
            System.out.println(stackBrand[top] + " " + stackInches[top] + " " + stackPrice[top]);
            deleteElement();
        }
    }
    public void stackCondition(){
        while (top != -1){
            if (stackInches[top] > 20){
                System.out.println(stackBrand[top] + " " + stackInches[top] + " " + stackPrice[top]);               
            } deleteElement();
        }
    }
    public int isFull() { return (top = mSize - 1); }
}
public class lab6{
    public static void main(String[] args){
        Stack mStack = new Stack(4);
        mStack.addElement(1, 19, 2500); mStack.addElement(2, 27, 2000);
        mStack.addElement(3, 20, 2700); mStack.addElement(4, 28, 3000);
        System.out.println("Стек до обработки: "); mStack.readStack(); mStack.isFull();
        System.out.println("Стек после обработки: "); mStack.stackCondition();
        System.out.println("##########################");
    }
}