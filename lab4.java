import java.util.Scanner;
public class lab4{
	private String[] teacher = {"Решетников", "Смыслова", "Морозова"},
					 cabinet = {"412", "128", "428"},
					 time	 = {"8:50 - 10:25", "10:40 - 12:15" , "13:15 - 14:50"},
					 lesson  = {"ТАиФЯ", "ТВиМС", "ИиП"};
	public void validation(int arraySize){
		if ((teacher.length == cabinet.length) && (cabinet.length == time.length) && (time.length == lesson.length) && (lesson.length == arraySize)){
			System.out.println("Валидация пройдена.");
		}
		else{
			System.out.println("Валидация не была пройдена.");
			System.exit(0);
		}
	}
	class innerClass{
		public void getTeacher(int i){
			System.out.print("Преподаватель: " + teacher[i] + ", ");
		}
		public void getCabinet(int i){
			System.out.print("Аудитория: " + cabinet[i] + ". ");
		}
		public void getTime(int i){
			System.out.print(time[i] + ", ");
		}
		public void getLesson(int i){
			System.out.print("Пара: " + lesson[i] + ", ");
		}
	}
	public static void main(String[] args){
		Scanner cin = new Scanner(System.in);
		System.out.print ("Введите количество пар: ");
		int arraySize = cin.nextInt();
		//Проверка на то, что количество элементов массива у всех одинаковое
		lab4 validator = new lab4(); validator.validation(arraySize); 
		lab4.innerClass inner = new lab4().new innerClass();
		for (int i = 0; i < arraySize; i++){
			inner.getTime(i);
			inner.getLesson(i);
			inner.getTeacher(i);
			inner.getCabinet(i);
			System.out.println();
		}
	}
}
