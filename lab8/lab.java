/*  Ввести массив, состоящий из 20 элементов целого типа больше 0. Создать два новых массива:
    в первый записать элементы исходного массива, которые больше 5, а во второй – остальные.
    Определить, в каком массиве больше сумма элементов. Обработать исключения:
    ошибочный ввод отрицательного или нулевого значения вместо положительного,
    а также выход за границы массива. */
import java.util.Scanner;
class lab{
    public static void main(String[] args){
        Scanner cin = new Scanner(System.in);
        int a[] = new int[20]; int b[] = new int[20]; int c[] = new int[20];
        int count1 = -1, count2 = -1, i;
        System.out.print("Введите 20 элементов для первого массива: ");
        for (i = 0; i < 20; ++i){
            a[i] = cin.nextInt();
            if (a[i] <= 0){ throw new IllegalArgumentException("Значение меньше единицы!"); }
            else{
                try{
                    if (a[i] < 5){ b[++count1] = a[i]; } 
                    if (a[i] > 5){ c[++count2] = a[i]; }
                } catch(ArrayIndexOutOfBoundsException e){
                    System.out.println(e.getMessage());
                }
            }
        }
        i = 0; 
        System.out.print("Второй массив, куда внесены элементы первого < 5: ") ;
        while (b[i] != 0){ System.out.print(b[i] + " "); ++i; }       
        i = 0; System.out.println();
        System.out.print("Третий массив, куда внесены элементы первого > 5: ");
        while (c[i] != 0){ System.out.print(c[i] + " "); ++i; }
        System.out.println();
    }
}