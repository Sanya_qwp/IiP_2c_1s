import java.util.Scanner;
class Calculator{
    public static double plus(double a, double b){
        return a+b;   
    }
    public static double minus(double a, double b){
        return a-b;
    }
    public static double multiply(double a, double b){
        return a*b;
    }
    public static double divide(double a, double b){
        return a/b;
    }
    public static void main(String[] args){
        Scanner cin = new Scanner(System.in); double a = 0, b = 0, c = 0, result = 0;
        System.out.print("Введите первую переменную: ");
            a = cin.nextDouble();
        System.out.print("Введите вторую переменную: ");
            b = cin.nextDouble();
        System.out.print("Введите действие, которое хотели бы совершить: ");
            c = cin.nextDouble();
        if(c == 1){
            result = plus(a, b);
        }
        if(c == 2){
            result = minus(a, b);
        }
        if(c == 3){     
            result = multiply(a, b);   
        }
        if(c == 4){
            result = divide(a, b);
        }
        System.out.println(result);
    }
}