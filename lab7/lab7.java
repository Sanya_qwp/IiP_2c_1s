import java.io.*;
import java.io.File;
import javax.swing.JOptionPane;

public class BackOrder {
    PrintWriter outputStream;
    BufferedReader inputStream;
    private String backOrderString = "";
    public static void main(String[] args) {
        BackOrder a = new BackOrder();
        File in = new File("E:/oscar/in.txt");
        try {
            a.backOrderTheFile(in);
        } 
        catch (FileNotFoundException e) {
            System.err.println("File not found");
        } catch (IOException e) {
            System.out.println("Что то не так. 22");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "File empty!");
        }
    }
    public void backOrderString(String str) {
        int i = str.length();
        while (i > 0) {
            backOrderString = backOrderString + str.charAt(i - 1);
            i--;
        }
    }
    public void backOrderTheFile(File newFile) throws FileNotFoundException, IOException {
        inputStream = new BufferedReader(new FileReader(newFile));
        String str = inputStream.readLine();
        backOrderString(str);
        while (str == null) {
            str = inputStream.readLine();
            backOrderString(str);
        }
        inputStream.close();
        outputStream = new PrintWriter(new FileOutputStream("E:/oscar/out.txt", true));
        outputStream.println(backOrderString);
        outputStream.close();
    }
}
