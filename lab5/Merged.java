abstract class Body{
	public abstract double volume();
	public abstract double area();
}
class Cone extends Body{
	double r, a, h;
	Cone(double r, double a, double h){ this.r = r; this.a = a; this.h = h;	}
	public double volume()  { return 0.33 * r * r * Math.PI * h; }
	public double area()	{ return Math.PI * r * r; }
	public String toString(){ return "Конус!"; }
}
class Parallelepiped extends Body{
	double a, b, h;
	Parallelepiped(double a, double b, double h){ this.a = a; this.b = b; this.h = h; }
	@Override
	public double volume()	{ return a * b * h; }
	@Override
	public double area()	{ return 2 * (a + b) * h + 2 * a * b; }
	@Override
	public String toString(){ return "Параллелепипед!"; }
}
class Merged{
	public static void main(String[] args){
		Cone cone = new Cone(5, 2, 5);
		System.out.println("Объем конуса: " + cone.volume() + ". Площадь конуса: " + cone.area());
		Parallelepiped parallelepiped = new Parallelepiped(5, 2, 5);
		System.out.println("Объем параллелепипеда: " + parallelepiped.volume() + ". Площадь параллелепипеда: " + parallelepiped.area());
		System.out.println(); System.out.println("#############################################################"); System.out.println(); 
	}
}